#### Список линтеров и форматтеров проекта:
1. ruff
2. black formatter
3. mypy

#### Настройка проекта
Основные настройки прописаны в **pyproject.toml**.
Для работы с репозиторием необходимо поставить pre-commit и poetry:
```bash
pip install pre-commit
pip install poetry
```

#### Работа с проектом
Для начала работы с проектом необходимо создать виртуальную среду .venv.
Если вы работаете в VS code, стоит поставить расширения [python](ms-python.python), [Ruff](charliermarsh.ruff), [MyPy](matangover.mypy), [Black formatter](ms-python.black-formatter).

Для установки зависимостей, прописываем следующую команду
```bash
poetry install
```

При коммите в репозиторий автоматически выполняется пре-коммит. Для внесения изменений в удаленный репозиторий исправьте ошибки пре-коммита.
