# mlops_train

## Описание проекта
В этом репозитории будут фиксироваться труды по прохождению курса [MLOps и production в DS исследованиях 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

## Методология ведения проекта
За неимением точного представления, что это будет за проект, предварительно условлюсь, что репозиторий будет вестись по методологии github flow. Подробнее о методологии [тут](https://habr.com/ru/articles/346066/).
При изменении зависимостей проекта, не забываем обновлять poetry.lock:
```bash
poetry lock
```

## Запуск проекта в контейнере
1.1 Билдим образ development
```bash
docker build . --target development -t mlops_dev
```

1.2 Билд образа production
```bash
docker build . --target production -t mlops_prod
```

2. Запускаем образ
2.1 Образ дева. Запускаем bash в интерактивной оболочке:
```bash
docker run -it mlops_dev
```

2.2 Пример запуска прод-образа с вольюмом:
```bash
docker run mlops_dev -v ./app: /opt/pysetup/app
```
